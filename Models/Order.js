const mongoose = require('mongoose')
const Product = require('./Models/Product')
const Users = require('./Models/Users')

const totalPrice = Product.price
const orderUser = Users.firstName 

const orderSchema = new mongoose.Schema({
		totalAmount:{
			total = totalPrice,
			required: [true, "Total amount is required"]
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		}
})
console.log(orderSchema)

module.exports = mongoose.model("Order", orderSchema)