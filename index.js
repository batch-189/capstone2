
//Require section 
const express = require('express')
const mongoose = require('mongoose')


//Port section
const PORT = process.env.PORT || 5000



const app = express()


//application section
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//Mongoose Connection 
mongoose.connect(`mongodb+srv://admin12345:admin123@zuitt-bootcamp.qquvkqu.mongodb.net/s37-s41?retryWrites=true&w=majority`,{
			useNewUrlParser: true,
			useUnifiedTopology: true
})

let db = mongoose.connection
db.on('error', () => console.error.bind(console, 'error'))
db.once(`open`, () => console.log(`Now connected in mongoDB Atlas`))



app.listen(PORT, () =>  {
	console.log(`API is now online on port ${PORT}`)
})


